var addbutton = document.querySelector(".add")
var editbutton = document.querySelector(".edit")
var getbutton = document.querySelector(".get")
var container = document.querySelector(".container")

//Adiciona elemntos
addbutton.addEventListener("click",e => {
    if(document.getElementById("nome").value == ""){
        document.getElementById("nome").classList.add("invalid")
    }else{
        document.getElementById("nome").classList.remove("invalid")
        var novo = document.createElement("h1")
        novo.className = "txt"; //classe para o css
        novo.innerHTML = document.getElementById("nome").value //conteúdo
        lugar = document.getElementById("lugar").value //posição se especificado
        var container = document.querySelector(".container")
        container.insertBefore(novo,container.children[lugar - 1]) //adicioando para a pos especificada ou ultimo se vazio
        document.querySelector(".namechange").classList.add("unset") // Retira a função de editar um elemento para evitar bugs
        removehighlight()
    }
})

//Remove elemnto clicado
container.addEventListener("click", e => {
    container.removeChild(e.target)
    document.querySelector(".namechange").classList.add("unset")// Retira a função de editar um elemento para evitar bugs
    removehighlight()
})

//escolhe elemnto para editar
getbutton.addEventListener("click", e => {
    removehighlight()
    var editlugar = document.getElementById("editlugar").value
    document.getElementById("num").value = editlugar
    document.querySelector(".hidden").innerHTML = "("+ editlugar +"°):" //Aciona a parte de edição de txt

    editlugar = parseInt(editlugar)         //subtrai 1 da pos escolhida mas mantém tipo de str
    editlugar -= 1
    editlugar = editlugar.toString()
    var txt = container.children[editlugar]

    //checa se elemento existe
    if(txt == undefined){
        document.getElementById("editlugar").classList.add("invalid") //muda cor do bg.
        document.querySelector(".namechange").classList.add("unset") // retira função de editar txt.
    }else{
        container.children[editlugar].classList.add("highlight")
        document.querySelector(".namechange").classList.remove("unset") //revert açõe do if acima
        document.getElementById("editlugar").classList.remove("invalid")
        var txt = container.children[editlugar].innerHTML //copia conteudo do elemento existente,
        document.getElementById("editnome").value = txt //põe dentro do input.
    }
    
})

//edita txt do elemento selecionado
editbutton.addEventListener("click", i =>{
    //invalido se vazio
    if(document.getElementById("editnome").value == ""){
        document.getElementById("editnome").classList.add("invalid") // muda cor do bg
    }else{
        document.getElementById("editnome").classList.remove("invalid") //reverte cor do bg

        //highlith o elemento selecionado

        var num = document.getElementById("num").value //posição do elemento porém mantém em forma de str
        num = parseInt(num)
        num -= 1
        num = num.toString()

        var editnome = document.getElementById("editnome").value //Muda o conteúdo
        container.children[num].innerHTML = editnome
    }
})


function removehighlight(){
    for(x of container.children){
        x.classList.remove("highlight")
    }
}

